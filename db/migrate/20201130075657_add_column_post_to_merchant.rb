class AddColumnPostToMerchant < ActiveRecord::Migration[6.0]
  def change
    add_column :merchants, :post, :text
  end
end
